# postgis

Creates a Singularity container based on the Docker Hub image mdillon/postgis which is PostGIS 2.4 with Postgres 10 under Alpine Linux. It's based on the official Postgres on Alpine image.

Pulling Built Image for Use
---------------------------
The Singularity images created are stored on research-singularity-registry.oit.duke.edu. You can pull your image down by curl (e.g. curl -O https://research-singularity-registry.oit.duke.edu/OIT-DCC/postgis.img).

Usage
-----

* To start the service, `singularity instance.start -B host_folder:/var/lib/postgresql/data postgres.img pginstance`(do not put a dash in the instance name).
* To pass variables for postgres database, use bind a file to `/postgresrc`.
  For example, `-B yourrc:/postgresrc`. Some common variables include `PGPORT` and `HOSTNAME`.
  (Due to some reason, the default postgres starts in `0.0.0.0`. For security reason, this recipe
  uses `HOSTNAME` which defaults to `localhost`.)
* Use `SINGULARITY_BINDPATH='host_folder:container_folder,host_file:container_file'` for easier binding.



Building Manually
-----------------

To build the image, run `sudo singularity build <name.img> Singularity`.
See [Singularity](https://singularity.lbl.gov/)
for more info.
